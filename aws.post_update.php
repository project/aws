<?php

/**
 * Ensure all AWS profiles have a default encryption profile set.
 */
function aws_post_update_encryption_profile_default() {
  /** @var \Drupal\aws\Entity\ProfileInterface[] $profiles */
  $profiles = \Drupal::entityTypeManager()
    ->getStorage('aws_profile')
    ->loadMultiple();

  foreach ($profiles as $profile) {
    if (!$profile->getEncryptionProfile()) {
      $profile
        ->setEncryptionProfile('_none')
        ->save();
    }
  }
}
